
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica61 {
    public static void main(String[] args) {
        Time time1 = new Time();
        Time time2 = new Time();
        
        time1.addJogador("Goleiro", new Jogador(1, "Bernardo"));
        time1.addJogador("Lateral", new Jogador(4, "Eduardo"));
        time1.addJogador("Atacante", new Jogador(7, "Paulo"));
        time2.addJogador("Goleiro", new Jogador(1, "Carlos"));
        time2.addJogador("Lateral", new Jogador(3, "Rafael"));
        time2.addJogador("Atacante", new Jogador(9, "Lucas"));
        
        System.out.println("Posição     Time 1     Time 2");
        System.out.println("Goleiro     "+time1.getJogadores().get("Goleiro")
                +"     "+time2.getJogadores().get("Goleiro"));
        System.out.println("Lateral     "+time1.getJogadores().get("Lateral")
                +"     "+time2.getJogadores().get("Lateral"));
        System.out.println("Atacante     "+time1.getJogadores().get("Atacante")
                +"     "+time2.getJogadores().get("Atacante"));
    }
}
